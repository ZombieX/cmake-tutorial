
#include<iostream>
#include<cmath>
#include<cstdlib>

#include"build/config.hpp"

int main(int argc, char* argv[]) {
	if (argc < 2) {

		std::cout
			<< argv[0] << " version "
			<< VERSION_MAJOR << "." << VERSION_MINOR << "\n";

		std::cout << "usage: " << argv[0] << " number\n";
		return 1;
	}

	auto input_v = std::atof(argv[1]);
	auto output_v = std::sqrt(input_v);

	std::cout
		<< "The square root of " << input_v
		<< " is " << output_v << " \n";

	return 0;
}